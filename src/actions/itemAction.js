export const addNewItem = (postData, count) => dispatch => {
  dispatch({
        type: "ADD_ITEM",
        payload: postData,
        count : count
      })
};