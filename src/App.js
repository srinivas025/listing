import React, { Component } from 'react';
import {connect} from 'react-redux';
import './App.css';
import List from './components/list';
import FinalTotal from './components/grandTotal';
import AddItem from './containers/addItem';

class App extends Component {

  constructor(props){
    super(props);
    this.state = {
      GrandTotal : 0
    }
  }
  componentDidMount(props){
    let t = 0;
    t +=this.props.total;
    this.setState({GrandTotal : t})
  }
  render() {
    const ListData  = this.props.ListData;
    const ListIndex = this.props.ListIndex;
    let GrandTotal=0;
    let ProductArr = [];
    Object.keys(ListData).map(key => {
      const item = ListData[key];    
      const total = item.qty * item.price;
      GrandTotal += total;
      ProductArr.push({product:item.product, qty:item.qty, price:item.price, total:total})   
    })
    return (
      <div className="App container">
      <AddItem />
       <List list= {ProductArr}/> 
       <h1>Grand Total : </h1>
       <FinalTotal total={GrandTotal} />
      </div>
    );
  }
}

function mapStateToProps(state){
  return{
    ListData : state.items.ListData,
    ListIndex : state.items.ListIndex
  }
}
export default connect(mapStateToProps, null)(App);
