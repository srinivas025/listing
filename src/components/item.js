import React, { Component } from 'react';

export default class Items extends Component{

	render(){
        return (
        	<tr>
        		
				<td>{this.props.product}</td>
				<td>{this.props.qty}</td>
				<td>{this.props.price}</td>
				<td>{this.props.total}</td>
            	
            </tr>
          );

	}
}