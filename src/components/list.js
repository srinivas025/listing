import React, { Component } from 'react';
import { Table } from 'reactstrap';
import Items from './item';

export default class List extends Component{
	render(){
		
    return (
      <div>
      <h1>Items</h1>
    <Table striped>
      <thead>
      <tr>
        <th>Product Name </th>
        <th>Quantity </th>
        <th>Price </th>
        <th> Total </th>
      </tr>
      </thead>
      <tbody>
        
          {this.props.list.map((item,i) => <Items key={i} product={item.product} qty={item.qty} price={item.price} total={item.total}/>)}
        
      </tbody>
    </Table>
    </div>
    )

	}
}