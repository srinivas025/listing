import ListDetails from '../components/code-challenge.json';
const initialState = {
  	// ListDetails,
  	ListIndex:ListDetails.list,
    ListData : ListDetails.details
};

export default function(state = initialState, action) {
  switch (action.type) {
    
    case "ADD_ITEM":
      return {
      	ListIndex: [ ...state.ListIndex, action.count],
        ListData: {
          ...state.ListData,
          [action.count]: action.payload
        }
      };
    
    default:
      return state;
  }
}