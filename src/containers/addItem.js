import React from 'react';
import { connect} from 'react-redux';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import {addNewItem} from '../actions/itemAction';
class AddItem extends React.Component {
	constructor(props){
		super(props);
		this.state = {
	      productName: '',
	      quantity: 0,
	      price : 0,
	      itemCount:6,
	      error : ''
	    };

		this.addNewItem = this.addNewItem.bind(this);
		this.onChange = this.onChange.bind(this);
	}

	addNewItem (e){
		e.preventDefault();
		const {productName, quantity, price, itemCount} = this.state;
		if((productName && quantity && price) == ''||0){
			this.setState({error : "Fields should not be empty"})
			return
		}
		if(isNaN(parseFloat(quantity)) && isFinite(quantity)){
			console.log("not a number");
			this.setState({error:"quantity should be a number"})
			return
		}
		if(isNaN(parseFloat(price)) && isFinite(price)){
			console.log("not a number");
			this.setState({error:"price should be a number"})
			return
		}
		this.setState(prevState => {
       return {itemCount: prevState.itemCount + 1}
    })
		const newItem = {
	      product:productName,
	      qty: quantity,
	      price : price

	    };

	    this.props.addNewItem(newItem, itemCount);
	    
	    this.setState({
	    	productName:'',
	    	quantity : '',
	    	price: '',
	    	error: ''
	    })
	}
	 onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

	render(){
		return(
			<div className="center_div">
			<h1> Add New Item</h1>
			<Form inline>
		        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
		          <Input type="text" name="productName" id="productname" onChange={this.onChange} placeholder="Product name" value={this.state.productName} />
		        </FormGroup>
		        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
		          <Input type="number" name="quantity" id="quantity" onChange={this.onChange} placeholder="Quantity" value={this.state.quantity} />
		        </FormGroup>
		        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
		          <Input type="number" name="price" id="price" onChange={this.onChange} placeholder="Price" value={this.state.price}/>
		        </FormGroup>
		        <Button onClick = {this.addNewItem}>Add</Button>
		        
		     </Form>
		     <div className="text-danger">{this.state.error}</div>
		     </div>
		)
	}
}
export default connect(null, { addNewItem })(AddItem);